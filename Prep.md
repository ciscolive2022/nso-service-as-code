# NSO Service as a Code

This is a "Network as Code" CICD pipeline which provisions sample service. In our case it is Layer2VPN P2P/VPWS into IOS-XE network via NSO.
In this example we will demonstrate how to use NSO to maintain network service configurations via a CICD pipeline, including support to create, update and delete operations on the service.

This repository hosts the customer service data in the [SaaC/L2VPN/](Saac/L2VPN/) directory defined in yaml files. Any changes to those files (actually to all files in the repo) will trigger a CICD pipeline which provisions the service(s) on the network.  
Only changes in master will provision the changes in the network, non-master branches trigger a so-called dry-run commit which only verifies the service data, but does not perform any changes on the network.

The main purpose of this is to show the power of NSO when used within a Network as Code use case. It's FASTMAP capabilities can keep the pipeline very simple as all the additions, changes and deletions are handled within NSO.

The pipeline invokes the Python script ([scripts/provision.py](scripts/provision.py)) which performs following steps:

1. Retrieves all currently provisioned customers with this service
2. Reads each service file (i.e. l2vpn/customer_01.yaml) and pushes the service data using a RESTCONF PUT request (with all service instances for this customer). This PUT requests triggers NSO to derive all the required changes and implements them in the network devices.
3. Executes test suite as defined in the robot test cases in [SaaC/L2VPN/test/l2vpn.robot](Saac/L2VPN/tests/l2vpn.robot)
4. Sends notification with test execution details to the Webex teams
5. Finally the script deletes all customers which have not been touched in step 2 (to cater for a deletion of a yaml file)

# Lab Preparation

This needs to be prepared before the workshop

Lab preparation requires following steps to be executed.

## Install Requirements

Requirements on the machine to support execution of the lab:
* docker version 20.10.11, build dea9396
* docker-compose version 1.29.2, build 5becea4c
* git version 2.33.0

## Create Branch and gitlab runner

Clone the particular branch for dedicated user and pull containers to prepare environment.

```
git clone https://gitlab.com/ciscolive2022/nso-service-as-code.git
cd nso-service-as-code
git checkout -b $username
cd setup
./docker-pull.sh
```

Run setup script to create containers:

```
cd nso-service-as-code/setup
./setup.sh -u $username
```

## Add Bot to the webex room

Add bot to the webex room by modifying scripts/config-prod.yaml

```
notify:
  # can specify room_id and/or WebexTeams person email
  room_id: Y2lzY29zcGFyazovL3VzL1JPT00vN2RiYmUxNDAtZTgyZC0xMWVjLWFkM2YtNTVkOTI4ZjJiYzc1
```

# Cleanup
```
docker-compose down
```

# Lab Execution

Click to go to [Lab Guide](Lab.md)
