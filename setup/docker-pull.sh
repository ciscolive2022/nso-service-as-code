#!/bin/bash

# Pull docker images
docker login registry.gitlab.com/ciscolive2022
docker pull registry.gitlab.com/ciscolive2022/nso-service-as-code
docker pull registry.gitlab.com/ciscolive2022/nso-l2vpn-service
docker pull gitlab/gitlab-runner:latest
