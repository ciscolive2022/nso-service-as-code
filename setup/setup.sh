#!/bin/bash

while getopts u: flag
do
    case "${flag}" in
        u) username=${OPTARG};;
    esac
done

# Create dedicated persitant docker volume for NSO pointing to your packages folder
# This volume is used in docker-compose.yml. Make sure the name of your volume matches
# docker volume create --driver local --opt type=none --opt device=/Users/$username/Documents/Cisco/CiscoLive/NSO-service-as-a-code/packages --opt o=bind cllv_nso_packages

# Optionally, if you wish gitlab-runner config to remain between docker-compose ups and downs, you need to create persistant volume
# This volume is used in docker-compose.yml. Make sure the name of your volume matches
# docker volume create gitlab-runner-config

# Start your local docker-compose environment
docker login registry.gitlab.com/ciscolive2022
docker-compose up -d

# Verify if all containers are running. Give it some time for the NSO container to fully start
docker ps

# The gitlab-runner will be dynamic, meaning that each branch will be associated with specific runner
# We will create userX git branch and the pipeline will be executed on userX runner
# This setup is only for the demo!!!!!!

# Chnage USER variable on each workshop laptop to achieve separation of environmentts
export GLUSER=$username
export TOKEN=GR1348941DXNR8QFteizxZCizYEq7

# now register the runner
docker exec gitlab-runner gitlab-runner register \
  --non-interactive                              \
  --name="Gitlab runner $GLUSER"                 \
  --url="https://gitlab.com/"                    \
  --registration-token="$TOKEN"                  \
  --tag-list $GLUSER                             \
  --run-untagged="false"                         \
  --executor="docker"                            \
  --docker-image="debian:buster"                 \
  --docker-network-mode="test_network"

# For preaparations you can also register a main runner that runs on main branch
# docker exec gitlab-runner gitlab-runner register \
#   --non-interactive                              \
#   --name="Gitlab runner main"                    \
#   --url="https://gitlab.com/"                    \
#   --registration-token="$TOKEN"                  \
#   --tag-list main                                \
#   --run-untagged="false"                         \
#   --executor="docker"                            \
#   --docker-image="debian:buster"                 \
#   --docker-network-mode="test_network"
