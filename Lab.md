
We have already completed [Setup](Prep.md). Go ahead with Lab Execution

# Lab Execution

Lab execution guide:

## Task 1 - Review Pipelines

1. Open in a new tab: https://gitlab.com/ciscolive2022/nso-service-as-code/-/pipelines

2. Review historical and current pipelines

3. Open in a new tab: https://gitlab.com/ciscolive2022/nso-service-as-code/-/ci/editor?branch_name=main

4. Review pipeline definition

## Task 2 - Lint Errors

Change nso-service-as-code/SaaC/L2VPN/customer.yaml service definition yaml file to trigger lint error

1. In terminal window to do the cheched out repository:
```
cd ~/nso-service-as-code
```

2. Edit Service as a Code config file:
```
vi SaaC/L2VPN/customer.yaml
```

2. Remove leading spaces from 'access-device' line
```
l2vpn:
  - name: test_service
access-device:
```

3. Exit editor
```
<ESC>
:wq!<ENTER>
```

4. Commit changes to branch $branch_name
```
git add SaaC/L2VPN/customer.yaml
git commit -m "Changes to customer.yaml"
git push --set-upstream origin $branch_name
```

5. Open in a new tab: https://gitlab.com/ciscolive2022/nso-service-as-code/-/pipelines

6. Review CI/CD process running with your user tag

## Task 3 - Fix lint error

Fix nso-service-as-code/SaaC/L2VPN/customer.yaml service definition - revert changes from Task 2.
Review of 'lint' stage

1. Edit Service as a Code config file:
```
vi SaaC/L2VPN/customer.yaml
```

2. Add 4 leading spaces from 'access-device' line
```
l2vpn:
  - name: test_service
    access-device:
```

3. Exit editor
```
<ESC>
:wq!<ENTER>
```

4. Commit changes to branch
```
git add SaaC/L2VPN/customer.yaml
git commit -m "Changes to customer.yaml"
git push
```

5. Open in a new tab: https://gitlab.com/ciscolive2022/nso-service-as-code/-/pipelines

6. Review CI/CD process running with your user tag - especially lint stage

## Task 4 - Triggering Config Changes

IP address change in the service.

1. Edit Service as a Code config file:
```
vi SaaC/L2VPN/customer.yaml
```

2. Change IP address
```
l2vpn:
  - name: test_service
    access-device:
    ...
      ip-address: 10.0.0.10
```

3. Exit editor
```
<ESC>
:wq!<ENTER>
```

4. Commit changes to branch
```
git add SaaC/L2VPN/customer.yaml
git commit -m "Changes to customer.yaml"
git push
```

5. Open in a new tab: https://gitlab.com/ciscolive2022/nso-service-as-code/-/pipelines

6. Review CI/CD process running with your user tag

## Task 5 - Address Validation

IP address change in the service.
Negative testcase - invalid IP address format.

1. Edit Service as a Code config file:
```
vi SaaC/L2VPN/customer.yaml
```

2. Change IP address
```
l2vpn:
  - name: test_service
    access-device:
    ...
      ip-address: 10.0.0.300
```

3. Exit editor
```
<ESC>
:wq!<ENTER>
```

4. Commit changes to branch
```
git add SaaC/L2VPN/customer.yaml
git commit -m "Changes to customer.yaml"
git push
```

5. Open in a new tab: https://gitlab.com/ciscolive2022/nso-service-as-code/-/pipelines

6. Review CI/CD process running with your user tag - especially Test and Rollback steps

## Task 6 - New Service Creation

1. Edit Service as a Code config file:
```
vi SaaC/L2VPN/customer.yaml
```

2. Uncomment following section:
```
  # - name: test_service2
  #   access-device:
  #     device-id: ios-1
  #     interface-id: 0/0/0/1
  #     ip-address: 10.0.0.11
  #     instance-id: 1000
  #     vlan-id: 200
  #   delivery-device:
  #     device-id: ios-2
  #     interface-id: 0/0/0/2
  #     ip-address: 10.0.0.2
  #     instance-id: 1000
  #     vlan-id: 200
  #   link:
  #     vc-id: 10012
  #     vc-class: 3
  #     qos-policy_name: test_name3
```

3. Revert IP address to correct:
```
l2vpn:
  - name: test_service
    access-device:
    ...
      ip-address: 10.0.0.2
```

4. Exit editor
```
<ESC>
:wq!<ENTER>
```

5. Commit changes to branch
```
git add SaaC/L2VPN/customer.yaml
git commit -m "Changes to customer.yaml"
git push
```

6. Open in a new tab: https://gitlab.com/ciscolive2022/nso-service-as-code/-/pipelines

7. Review CI/CD process running with your user tag - especially Test and Rollback steps

## Task 7 - Service removal

1. Edit Service as a Code config file:
```
vi SaaC/L2VPN/customer.yaml
```

2. Comment following section:
```
  - name: test_service2
    access-device:
      device-id: ios-1
      interface-id: 0/0/0/1
      ip-address: 10.0.0.11
      instance-id: 1000
      vlan-id: 200
    delivery-device:
      device-id: ios-2
      interface-id: 0/0/0/2
      ip-address: 10.0.0.2
      instance-id: 1000
      vlan-id: 200
    link:
      vc-id: 10012
      vc-class: 3
      qos-policy_name: test_name3
```

3. Exit editor
```
<ESC>
:wq!<ENTER>
```

4. Commit changes to branch
```
git add SaaC/L2VPN/customer.yaml
git commit -m "Changes to customer.yaml"
git push
```

5. Open in a new tab: https://gitlab.com/ciscolive2022/nso-service-as-code/-/pipelines

6. Review CI/CD process running with your user tag - especially Test and Rollback steps
